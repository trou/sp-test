# frozen_string_literal: true

module LogAnalyser
  class Entry
    attr_reader :path, :address
    def initialize(path:, address:)
      @path = path
      @address = address
    end

    def self.from_string(string)
      elements = string.split(' ')
      new(path: elements[0], address: elements[1])
    end
  end
end
