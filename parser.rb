#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'lib/log_analyser'

raise ArgumentError, 'missing argument' if ARGV.length != 1
raise ArgumentError, 'source does not exist' unless File.exist?(ARGV[0])

source = ARGV[0]

parser = LogAnalyser::Parser.new
parser.call(source: source)
parser.print_stats
