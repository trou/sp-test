# frozen_string_literal: true

require_relative 'spec_helper'

describe LogAnalyser::Stats do
  subject { described_class.new(entries: entries) }
  let(:entries) {
    [
      LogAnalyser::Entry.from_string('/path 127.0.0.1'),
      LogAnalyser::Entry.from_string('/path 127.0.0.1'),
      LogAnalyser::Entry.from_string('/path2 127.0.0.2')
    ]
  }

  describe '#unique_visits' do
    it 'calculates correct values for paths' do
      expect(subject.unique_visits['/path']).to eq 1
      expect(subject.unique_visits['/path2']).to eq 1
    end
  end

  describe '#views' do
    it 'calculates correct values for paths' do
      expect(subject.views['/path']).to eq 2
      expect(subject.views['/path2']).to eq 1
    end
  end
end
