# frozen_string_literal: true

module LogAnalyser
  class Log
    attr_reader :entries
    extend Forwardable

    def initialize
      @entries = []
    end

    def <<(elem)
      @entries << elem
    end

    def_delegators :stats, :unique_visits, :views

    private

    def stats
      @stats ||= Stats.new(entries: @entries)
    end
  end
end
