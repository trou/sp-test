# frozen_string_literal: true

module LogAnalyser
  class Stats
    def initialize(entries:)
      @entries = entries.each_with_object({}) do |entry, hash|
        hash[entry.path] = [] unless hash.key?(entry.path)
        hash[entry.path] << entry.address
      end
    end

    def unique_visits
      count_values(unique: true)
    end

    def views
      count_values
    end

    private

    def count_values(unique: false)
      @entries.transform_values do |value|
        unique ? value.uniq.length : value.length
      end
    end
  end
end
