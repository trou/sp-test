# frozen_string_literal: true

require_relative 'spec_helper'

describe LogAnalyser::Parser do
  subject { described_class.new }
  let(:source) { 'spec/fixtures/webserver.log' }
  let(:log_lines) do
    [
      '/line1 1.2.3.4',
      '/line2 4.3.2.1',
    ]
  end

  describe '#call' do
    subject { described_class.new }
    let(:log) { LogAnalyser::Log }
    before do
      allow(subject).to receive(:lines).with(source).and_return(log_lines)
    end

    it 'calls log << twice' do
      expect_any_instance_of(log).to receive(:<<).twice
      subject.call(source: source)
    end
  end

  describe '#print_stats' do
    it 'calls formatter' do
      expect(subject).to receive(:format_unique_visits).once
      expect(subject).to receive(:format_views).once
      subject.print_stats
    end
  end
end
