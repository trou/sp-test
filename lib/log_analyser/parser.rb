module LogAnalyser
  class Parser
    def initialize(log: LogAnalyser::Log.new)
      @log = log
    end

    def call(source:)
      lines(source).each do |line|
        @log << LogAnalyser::Entry.from_string(line)
      end
    end

    def print_stats
      puts 'Unique visits'
      puts format_unique_visits
      puts 'Views'
      puts format_views
    end

    private
    
    def lines(source)
      IO.foreach(source)
    end

    def format_unique_visits
      StatsFormatter.new(stats: @log.unique_visits, suffix: 'unique visits').format
    end

    def format_views
      StatsFormatter.new(stats: @log.views, suffix: 'views').format
    end
  end
end
