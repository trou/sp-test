FROM ruby:2.5

RUN bundle config --global frozen 1
RUN mkdir /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

CMD ["rspec"]