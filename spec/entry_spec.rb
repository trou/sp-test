# frozen_string_literal: true

require_relative 'spec_helper'

describe LogAnalyser::Entry do
  let(:line) { '/home 321.321.321.321' }
  let(:path) { '/path' }
  let(:address) { '123.123.123.123' }

  describe '#new' do
    subject { described_class.new(path: path, address: address) }
    context 'for parameters given' do
      it 'creates an entry' do
        expect(subject.path).to eq('/path')
        expect(subject.address).to eq('123.123.123.123')
      end
    end
  end

  describe '.from_string' do
    subject { described_class.from_string(line) }
    context 'for line given' do
      it 'creates an entry' do
        expect(subject.path).to eq('/home')
        expect(subject.address).to eq('321.321.321.321')
      end
    end
  end
end
