# frozen_string_literal: true

require_relative 'spec_helper'

describe LogAnalyser::Log do
  subject { described_class.new }
  let(:entry) do
    LogAnalyser::Entry.from_string('/home 321.321.321.321')
  end

  describe '#<<' do
    context 'for single entry' do
      before do
        subject << entry
      end
      it 'returns proper collection' do
        expect(subject.entries.length).to eq 1
        expect(subject.entries).to eq([entry])
      end
    end

    context 'for multiple entries' do
      before do
        3.times do
          subject << entry
        end
      end
      it 'returns proper collection' do
        expect(subject.entries.length).to eq 3
        expect(subject.entries).to eq([entry, entry, entry])
      end
    end

    context 'delegated methods' do
      describe '#unique_visits' do
        it 'call correct class' do
          expect(subject). to receive(:stats).and_call_original
          subject.unique_visits
        end
      end
      describe '#views' do
        it 'call correct class' do
          expect(subject). to receive(:stats).and_call_original
          subject.views
        end
      end
    end
  end
end
