# frozen_string_literal: true

module LogAnalyser
  class StatsFormatter
    DEFAULT_SEPARATOR = "\n"
    DEFAULT_PREFIX = ' '

    def initialize(stats:, suffix:, prefix: DEFAULT_PREFIX, separator: DEFAULT_SEPARATOR)
      @stats = stats
      @suffix = suffix
      @separator = separator
      @prefix = prefix
    end

    def format
      @stats.sort_by { |_, v| v }.reverse.map do |k, v|
        format_element(k, v)
      end.join(@separator)
    end

    private

    def format_element(element_key, element_value)
      "#{@prefix}#{element_key} #{element_value} #{@suffix}"
    end
  end
end
