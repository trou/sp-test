# frozen_string_literal: true

require_relative 'spec_helper'

describe LogAnalyser::StatsFormatter do
  subject { described_class.new(stats: stats, suffix: suffix) }
  let(:stats) {
    {
      "/path" => 1,
      "/home" => 3,
    }
  }
  let(:suffix) { 'super views' }

  describe '#format' do
    it '' do
      expect(subject.format).to eq " /home 3 super views\n /path 1 super views"
    end
  end
end
